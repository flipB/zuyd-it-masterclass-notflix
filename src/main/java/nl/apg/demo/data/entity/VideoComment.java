package nl.apg.demo.data.entity;

import javax.persistence.*;
import java.util.UUID;

@Entity
public class VideoComment {
    @Id
    private String id;
    private String commentText;
    private String commentUser;
    private String timestamp;
    private Integer likes;
    @ManyToOne
    private VideoItem video;

    @PrePersist
    void generateId() {
        id = UUID.randomUUID().toString();
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getCommentUser() {
        return commentUser;
    }

    public void setCommentUser(String commentUser) {
        this.commentUser = commentUser;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public VideoItem getVideo() {
        return video;
    }

    public void setVideo(VideoItem video) {
        this.video = video;
    }
}
