package nl.apg.demo.data.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserInfo {

	@Id
	private String id;
	private String userName;

	public String getId() {
		return id;
	}

	public String getUserName() {
		return userName;
	}

}
