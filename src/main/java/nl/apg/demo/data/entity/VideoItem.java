package nl.apg.demo.data.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import java.util.UUID;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class VideoItem {

	@Id
	private String id;

	private String title;

	private String subTitle;

	private String imageSrc;

	private String videoSrc;

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "video", cascade = CascadeType.ALL, orphanRemoval = true)
	private List<VideoComment> commentList;

	@PrePersist
	void generateId() {
		id = UUID.randomUUID().toString();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getId() {
		return id;
	}

	public String getImageSrc() {
		return imageSrc;
	}

	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public String getVideoSrc() {
		return videoSrc;
	}

	public void setVideoSrc(String videoSrc) {
		this.videoSrc = videoSrc;
	}

	public List<VideoComment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<VideoComment> commentList) {
		this.commentList = commentList;
	}

	@Override
	public String toString() {
		return "VideoItem [id=" + id + ", title=" + title + "]";
	}

}
