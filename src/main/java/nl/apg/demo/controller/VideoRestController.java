package nl.apg.demo.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import nl.apg.demo.data.entity.VideoDescription;
import nl.apg.demo.data.entity.VideoItem;
import nl.apg.demo.service.VideoDescriptionService;
import nl.apg.demo.service.VideoItemService;

@RestController
@RequestMapping("/api/video")
public class VideoRestController {
	@Autowired
	private VideoItemService videoService;
	@Autowired
	private VideoDescriptionService detailService;

	@GetMapping
	public List<VideoItem> get(@RequestParam(value = "query", required = false) String query) {
		if (query != null && !query.isEmpty()) {
			return videoService.searchVideo(query);
		} else {
			return videoService.getFirstTenItems();
		}
	}

	@GetMapping("/{id}")
	public VideoDescription getOne(@PathVariable String id) {
		return detailService.getVideoDescription(id);
	}

	@PostMapping
	public VideoDescription saveOne(@RequestBody VideoDescription video) {
		detailService.saveVideoDescription(video);
		return video;
	}
}
