package nl.apg.demo.controller;

import nl.apg.demo.data.entity.VideoComment;
import nl.apg.demo.data.repository.VideoItemRepository;
import nl.apg.demo.service.VideoCommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/video")
public class VideoCommentRestController {
    @Autowired
    private VideoCommentService commentService;
    @Autowired
    private VideoItemRepository videoRepo;

    @PostMapping("/{id}/comment")
    public VideoComment saveComment(@RequestBody VideoComment comment, @PathVariable String id) {
        if (videoRepo.findById(id) != null) {
            // TODO: User meegeven aan de comment
            // TODO: Timestamp meegeven aan de comment
            comment.setVideo(videoRepo.getOne(id));
            commentService.saveVideoComment(comment);
        } else {
            // TODO: Error-Page tonen
        }
        return comment;
    }
}
