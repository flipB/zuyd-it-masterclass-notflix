package nl.apg.demo.service;

import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import nl.apg.demo.data.entity.VideoItem;
import nl.apg.demo.data.repository.VideoItemRepository;

@Service
public class VideoItemService {
	@Autowired
	private TagService tagService;

	@Autowired
	private VideoItemRepository videoRepo;

	public List<VideoItem> getFirstTenItems() {
		return videoRepo.findTop10By();
	}

	public List<VideoItem> searchVideo(String query) {
		Set<String> keys = tagService.getVideoSelection(query);
		return videoRepo.findByIdIn(keys);
	}

}
